﻿using Application.ViewModels;

namespace Application.Utils
{
    public static class CartItemExtension
    {
        public static CartItem ToCartItem(this CartItemDTO cartItemAddDTO)
        {
            return new CartItem
            {
                UserId = cartItemAddDTO.UserId,
                BookId = cartItemAddDTO.BookId,
                Quantity = cartItemAddDTO.Quantity
            };
        }

        public static List<CartItemViewModel> ToCartItemViewModelList(this List<CartItem> cartItems)
        {
            var result = new List<CartItemViewModel>();

            foreach (var cartItem in cartItems)
            {
                var cartItemViewModel = new CartItemViewModel
                {
                    Id = cartItem.Id,
                    BookName = cartItem.Book.Title,
                    UserName = cartItem.User.Name,
                    Quantity = cartItem.Quantity
                };

                result.Add(cartItemViewModel);
            }

            return result;
        }
    }
}
