﻿using Application.Commons;
using Application.Interfaces;
using Domain.Entities;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Application.Utils
{
    public class EmailUtils : IEmailUtils
    {
        private readonly AppConfiguration _appConfiguration;

        public EmailUtils()
        {
        }

        public EmailUtils(AppConfiguration appConfiguration)
        {
            _appConfiguration = appConfiguration ?? throw new ArgumentNullException(nameof(appConfiguration));

        }

        public void SendEmailAsync(string recipientEmail, string purpose)
        {
            string emailForSend = _appConfiguration.EmailSection.EmailString;
            string appPasswordConfiguration = _appConfiguration.EmailSection.AppPassword;

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var emailMessage = new MailMessage()
            {
                From = new MailAddress(emailForSend),
            };

            if (purpose == "PaymentSuccess")
            {
                emailMessage.Subject = _appConfiguration.EmailSection.PaymentSubject;
                emailMessage.Body = _appConfiguration.EmailSection.PaymentBody;
            }
            else if (purpose == "ForgotPassword")
            {
                emailMessage.Subject = _appConfiguration.EmailSection.OTPSubject;
                emailMessage.Body = _appConfiguration.EmailSection.OTPBody + GenerateOTP();
            }

            emailMessage.To.Add(new MailAddress(recipientEmail));
            smtpClient.Send(emailMessage);
        }


        public static OTP GetValidOTP(string email, string otp, IUnitOfWork _unitOfWork, ICurrentTime _currentTime)
        {
            // Kiểm tra xem mã OTP có hợp lệ không
            //Su dung caching
            var validOTP = _unitOfWork.OTPRepository.GetOTPByEmailAndCode(email, otp);

            if (validOTP != null)
            {
                // Kiểm tra xem mã OTP đã hết hạn chưa
                if (validOTP.ExpirationTime >= _currentTime.GetCurrentTime())
                {
                    return validOTP; // Mã OTP hợp lệ
                }
            }

            throw new Exception("Invalid OTP"); // Mã OTP không hợp lệ hoặc đã hết hạn
        }

        private static Random random = new Random();

        public static string GenerateOTP()
        {
            const string chars = "0123456789";

            StringBuilder otp = new StringBuilder(6);
            for (int i = 0; i < 6; i++)
            {
                otp.Append(chars[random.Next(chars.Length)]);
            }

            return otp.ToString();
        }
    }
}
