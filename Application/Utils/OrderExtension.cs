﻿using Application.DTO;
using Application.ViewModels;

namespace Application.Utils
{
    public static class OrderExtension
    {
        public static Order ToOrder(this OrderDTO orderDTO)
        {
            return new Order
            {
                PaidDate = orderDTO.PaidDate,
                TotalPrice = orderDTO.TotalPrice,
            };
        }

        public static List<OrderDetail> ToOrderDetail(this List<OrderDetailDTO> orderDetailDTO)
        {
            return orderDetailDTO.Select(x => new OrderDetail
            {
                OrderId = x.OrderId,
                BookId = x.BookId,
                Quantity = x.Quantity,
            }).ToList();
        }
    }
}
