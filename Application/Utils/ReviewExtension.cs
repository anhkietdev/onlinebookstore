﻿using Application.DTO;

namespace Application.Utils
{
    public static class ReviewExtension
    {
        public static Review ToAddReview(this ReviewDTO reviews)
        {
            return new Review
            {
                BookId = reviews.BookId,
                UserId = reviews.UserId,
                Content = reviews.Content,
                Image = reviews.Image,
                Rating = reviews.Rating,
            };
        }

        public static List<ReviewViewModel> ToReviewViewModel(this List<Review> reviews)
        {
            return reviews.Select(x => new ReviewViewModel
            {
                Id = x.Id,
                BookId = x.BookId,
                UserId = x.UserId,
                Content = x.Content,
                Image = x.Image,
                Rating = x.Rating,
            }).ToList();
        }
    }
}
