﻿using Application.ViewModels;

namespace Application.Utils
{
    public static class BookExtension
    {
        public static List<BookViewModel> ToBookViewModel(this List<Book> books)
        {
            return books.Select(x => new BookViewModel
            {
                Id = x.Id,
                Author = x.Author,
                Title = x.Title,
                PublicationDate = x.PublicationDate,
                Genre = x.Genre,
                PricePerUnit = x.PricePerUnit,
            }).ToList();
        }

        public static List<Book> ToBookAddDTO(this List<BookDTO> books)
        {
            return books.Select(x => new Book
            {
                Title = x.Title,
                Author = x.Author,
                PublicationDate = x.PublicationDate,
                Genre = x.Genre,
                StockQuantity = x.StockQuantity,
                PricePerUnit = x.PricePerUnit,
            }).ToList();
        }

        public static void UpdateFromBookDTO(this Book book, BookDTO bookDTO, DateTime modifiedOn)
        {
            book.Title = string.IsNullOrEmpty(bookDTO.Title) ? book.Title : bookDTO.Title;
            book.Author = string.IsNullOrEmpty(bookDTO.Author) ? book.Author : bookDTO.Author;
            book.PublicationDate = bookDTO.PublicationDate == default ? book.PublicationDate : bookDTO.PublicationDate;
            book.StockQuantity = bookDTO.StockQuantity == default ? book.StockQuantity : bookDTO.StockQuantity;
            book.PricePerUnit = bookDTO.PricePerUnit == default ? book.PricePerUnit : bookDTO.PricePerUnit;
            book.ModifiedOn = modifiedOn;
        }
    }
}
