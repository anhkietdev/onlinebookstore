﻿using Application.DTO;
using System.Runtime.CompilerServices;

namespace Application.Utils
{
    public static class FavoriteBookExtension
    {
        public static FavoriteBook ToAddFavoriteBook(this FavoriteBookDTO book)
        {
            return new FavoriteBook
            {
                BookId = book.BookId,
                UserId = book.UserId,
            };
        }

        public static List<FavoriteBookViewModel> ToFavoriteBookView(this List<FavoriteBook> favoriteBooks)
        {
            return favoriteBooks.Select(book => new FavoriteBookViewModel
            {
                Id = book.Book.Id,
                Title = book.Book.Title,
                Author = book.Book.Author,  
                PublicationDate = book.Book.PublicationDate,
                PricePerUnit = book.Book.PricePerUnit,
                Genre = book.Book.Genre,
            }).ToList();
        }
    }
}
