﻿using Application.ViewModels;

namespace Application.Interfaces
{
    public interface ICartItemService
    {
        Task AddBookToCart(CartItemDTO cartItemAddDTO);
        Task RemoveBookFromCart(CartItemDTO cartItemDTO);
        List<CartItemViewModel> ViewCartItemDetail(int userId);

    }
}
