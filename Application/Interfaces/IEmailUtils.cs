﻿namespace Application.Interfaces
{
    public interface IEmailUtils
    {
        void SendEmailAsync(string recipientEmail, string purpose);
    }
}
