﻿using Application.DTO;

namespace Application.Interfaces
{
    public interface IOrderService
    {
        Task<Order> ProcessPayment(int userId, OrderDTO orderDTO);
        Task AddToOrderDetail(int userId, Order order);
    }
}
