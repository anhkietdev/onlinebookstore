﻿namespace Application.Interfaces
{
    public interface IDailyReportService
    {
        Task GenerateDailyReport(); // Tạo báo cáo cuối ngày
    }
}
