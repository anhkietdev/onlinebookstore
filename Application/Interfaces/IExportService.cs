﻿namespace Application.Interfaces
{
    public interface IExportService
    {
        byte[] ExportReportToBytes(int reportData);
    }

}
