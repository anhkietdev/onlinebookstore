﻿using Microsoft.AspNetCore.Http;

namespace Application.Interfaces
{
    public interface IClaimsService
    {
        int GetCurrentUserId(HttpContext httpContext);
    }
}
