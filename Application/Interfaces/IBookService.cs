﻿using Application.Commons;
using Application.ViewModels;
using Domain.Enums;

namespace Application.Interfaces
{
    public interface IBookService
    {
        Task AddAsync (BookDTO book);
        Task UpdateBookAsync(int id, BookDTO book);
        Task SoftDelete(int bookId);
        Task HardDelete(int bookId);
        Task<List<Book>> GetBooksAsync();
        List<BookViewModel> GetBooksByAuthorAsync(string author);
        List<BookViewModel> GetBooksByTitleAsync(string title);
        List<BookViewModel> GetBooksByGenreAsync(BookGenre genre);
        List<BookViewModel> GetBooksByPublicationDateAsync(DateTime publicationDate);
    }
}
