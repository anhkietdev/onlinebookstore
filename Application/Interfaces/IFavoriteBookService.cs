﻿using Application.DTO;

namespace Application.Interfaces
{
    public interface IFavoriteBookService
    {
        Task Add(FavoriteBookDTO favoriteBookDTO);
        Task Remove(FavoriteBookDTO favoriteBookDTO);
        Task<List<FavoriteBookViewModel>> ViewFavoriteBook(int userId);
    }
}
