﻿using Application.DTO;

namespace Application.Interfaces
{
    public interface IReviewService
    {
        Task AddReview(ReviewDTO reviewDTO);
        Task<List<ReviewViewModel>> ViewReviews(int bookId);
    }
}
