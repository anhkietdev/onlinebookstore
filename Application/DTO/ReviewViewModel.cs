﻿namespace Application.DTO
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public int UserId { get; set; }
        public string Image { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
    }
}
