﻿namespace Application.DTO
{
    public class FavoriteBookDTO
    {
        public int BookId { get; set; }
        public int UserId { get; set; }
    }
}
