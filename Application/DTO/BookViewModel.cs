﻿using Domain.Enums;

namespace Application.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublicationDate { get; set; }
        public BookGenre Genre { get; set; }
        public int PricePerUnit { get; set; }
    }
}
