﻿namespace Application.DTO
{
    public class OrderDTO
    {
        public DateTime PaidDate { get; set; }
        public int TotalPrice { get; set; }
    }
}
