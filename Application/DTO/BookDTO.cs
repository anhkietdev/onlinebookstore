﻿using Domain.Enums;

namespace Application.ViewModels
{
    public class BookDTO : BaseEntity
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublicationDate { get; set; }
        public int StockQuantity { get; set; }
        public int PricePerUnit { get; set; }
        public BookGenre Genre { get; set; }
    }
}
