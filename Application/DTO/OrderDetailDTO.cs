﻿namespace Application.DTO
{
    public class OrderDetailDTO
    {
        public int OrderId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }
    }
}
