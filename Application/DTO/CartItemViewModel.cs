﻿namespace Application.ViewModels
{
    public class CartItemViewModel
    {
        public int Id { get; set; } 
        public string BookName { get; set; }
        public string UserName { get; set; }
        public int Quantity { get; set; }
    }
}
