﻿namespace Application.DTO
{
    public class ReviewDTO
    {
        public int BookId { get; set; }
        public int UserId { get; set; }
        public string Image { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
    }
}
