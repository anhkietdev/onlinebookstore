﻿using Application.DTO;
using Application.Interfaces;
using Application.Utils;

namespace Application.Services
{
    public class FavoriteBookService : IFavoriteBookService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FavoriteBookService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task Add(FavoriteBookDTO favoriteBookDTO)
        {
            var user = await _unitOfWork.UserRepository.FindEntityAsync(x => x.Id == favoriteBookDTO.UserId);
            if (user == null)
            {
                throw new Exception("User not found!");
            }

            var book = await _unitOfWork.BookRepository.FindEntityAsync(x => x.Id == favoriteBookDTO.BookId);
            if (book is null)
            {
                throw new Exception("Book not found!");
            }

            var favoriteBookIsExist = await _unitOfWork.FavoriteBookRepository.FindEntityAsync(x => x.UserId == favoriteBookDTO.UserId && x.BookId == favoriteBookDTO.BookId);
            if (favoriteBookIsExist is not null)
            {
                throw new Exception("Favorite book is existed!");
            }

            var bookAdd = favoriteBookDTO.ToAddFavoriteBook();

            await _unitOfWork.FavoriteBookRepository.AddAsync(bookAdd);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task Remove(FavoriteBookDTO favoriteBookDTO)
        {
            var user = await _unitOfWork.FavoriteBookRepository.FindEntityAsync(x => x.UserId == favoriteBookDTO.UserId);
            if (user is null)
            {
                throw new Exception("User not found!");
            }

            var book = await _unitOfWork.FavoriteBookRepository.FindEntityAsync(x => x.BookId == favoriteBookDTO.BookId);
            if (book is null)
            {
                throw new Exception($"Favorite Book {favoriteBookDTO.BookId} not found!");
            }

            _unitOfWork.FavoriteBookRepository.SoftDelete(book);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<List<FavoriteBookViewModel>> ViewFavoriteBook(int userId)
        {
            var  user = await _unitOfWork.FavoriteBookRepository.FindEntityAsync(x => x.UserId == userId);
            if (user is null)
            {
                throw new Exception("User not found!");
            }

            var book = _unitOfWork.FavoriteBookRepository.ViewAllFavoriteBooks(userId);
            if (book is null) 
            {
                throw new Exception("Book not found!");
            }

            var bookView = book.ToFavoriteBookView();
            if (bookView.Count == 0)
            {
                throw new Exception("There is nothing in your wish list");
            }
            return bookView;
        }
    }
}
