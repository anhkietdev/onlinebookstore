﻿using Application.DTO;
using Application.Interfaces;
using Application.Utils;

namespace Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailUtils _emailUtils;

        public OrderService(IUnitOfWork unitOfWork, IEmailUtils emailUtils)
        {
            _unitOfWork = unitOfWork;
            _emailUtils = emailUtils;
        }

        public async Task AddToOrderDetail(int userId, Order order)
        {
            var user = await _unitOfWork.UserRepository.FindEntityAsync(x => x.Id == userId);

            if (user == null)
            {
                throw new Exception("User not found!");
            }

            var cartItems =  _unitOfWork.CartItemRepository.ViewCartDetail(userId);
            List<OrderDetailDTO> orderDetailDTOs = new List<OrderDetailDTO>();

            foreach (var item in cartItems)
            {
                OrderDetailDTO orderDetailDTO = new OrderDetailDTO();

                orderDetailDTO.OrderId = order.Id;
                orderDetailDTO.BookId = item.BookId;
                orderDetailDTO.Quantity = item.Quantity;

                orderDetailDTOs.Add(orderDetailDTO);
            }

            var listDetail = orderDetailDTOs.ToOrderDetail();

            await _unitOfWork.OrderDetailRepository.AddRangeAsync(listDetail);
            _unitOfWork.CartItemRepository.SoftDeleteRange(cartItems);

            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<Order> ProcessPayment(int userId, OrderDTO orderDTO)
        {
            var user = await _unitOfWork.UserRepository.FindEntityAsync(x => x.Id == userId);
            if (user is null)
            {
                throw new Exception("UserID not found!");
            }

            var userCart = _unitOfWork.CartItemRepository.ViewCartDetail(userId);
            foreach (var item in userCart)
            {
                orderDTO.TotalPrice = item.Book.PricePerUnit * item.Quantity;
            }

            if (user.WalletAmount < orderDTO.TotalPrice)
            {
                throw new Exception("You cannot afford this Total Price. Please recharge your wallet");
            }

            var order = orderDTO.ToOrder();

            order.UserId = userId;
            order.PaidDate = DateTime.Now;

            user.WalletAmount -= order.TotalPrice;

            await _unitOfWork.OrderRepository.AddAsync(order);
            _emailUtils.SendEmailAsync(user.Email, "PaymentSuccess");
            await _unitOfWork.SaveChangesAsync();

            return order;
        }
    }
}
