﻿using Application.Commons;
using Application.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System.Net;
using System.Net.Mail;

namespace Application.Services
{
    public class DailyReportService : IDailyReportService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppConfiguration _appConfiguration;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public DailyReportService(IUnitOfWork unitOfWork, AppConfiguration appConfiguration, IWebHostEnvironment hostingEnvironment)
        {
            _unitOfWork = unitOfWork;
            _appConfiguration = appConfiguration;
            _hostingEnvironment = hostingEnvironment;
        }

        public async Task GenerateDailyReport()
        {
            int orderCount = await _unitOfWork.OrderRepository.GetOrderCountForToday();
            int paidOrderCount = await _unitOfWork.OrderRepository.GetPaidOrderCount();
            int totalRevenue = await _unitOfWork.OrderRepository.TotalRevenue();
            int newAccountCount = await _unitOfWork.UserRepository.NewAccountCount();

            string reportContent = GenerateReportContent(orderCount, paidOrderCount, totalRevenue, newAccountCount);

            string reportFileName = "daily_report.pdf";
            string reportFolderName = "reports";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string reportPath = Path.Combine(webRootPath, reportFolderName, reportFileName);

            await CreatePdfFromHtml(reportContent, reportPath);

            await SendEmailAsync(_appConfiguration.EmailSection.EmailString, reportPath);
        }

        private string GenerateReportContent(int orderCount, int paidOrderCount, int totalRevenue, int newAccountCount)
        {
            string reportContent = $@"
            <html>
            <body>
                <h1>Daily Report</h1>
                <p>Total Orders: {orderCount}</p>
                <p>Paid Orders: {paidOrderCount}</p>
                <p>Total Revenue: ${totalRevenue}</p>
                <p>New Accounts Created Today: {newAccountCount}</p>
            </body>
            </html>
        ";

            return reportContent;
        }

        private async Task CreatePdfFromHtml(string htmlContent, string pdfFilePath)
        {
            ChromePdfRenderer renderer = new ChromePdfRenderer();
            using (PdfDocument pdfDocument = await renderer.RenderHtmlAsPdfAsync(htmlContent))
            {
                pdfDocument.SaveAs(pdfFilePath);
            }
        }

        private async Task SendEmailAsync(string recipientEmail, string path)
        {
            string emailForSend = _appConfiguration.EmailSection.EmailString;
            string appPasswordConfiguration = _appConfiguration.EmailSection.AppPassword;

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var emailMessage = new MailMessage()
            {
                From = new MailAddress(emailForSend),
            };

            emailMessage.Subject = _appConfiguration.EmailSection.ReportSubject;
            emailMessage.Body = _appConfiguration.EmailSection.ReportBody;

            emailMessage.To.Add(new MailAddress(recipientEmail));

            Attachment attachment = new Attachment(path, System.Net.Mime.MediaTypeNames.Application.Pdf);
            emailMessage.Attachments.Add(attachment);

            await smtpClient.SendMailAsync(emailMessage);
        }
    }
}
