﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels;
using AutoMapper;
using Domain.Enums;

namespace Application.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICurrentTime _currentTime;

        public BookService(IUnitOfWork unitOfWork, ICurrentTime currentTime)
        {
            _unitOfWork = unitOfWork;
            _currentTime = currentTime;
        }

        public async Task AddAsync(BookDTO bookAddDTO)
        {
            try
            {
                var bookList = new List<BookDTO> { bookAddDTO };
                var books = bookList.ToBookAddDTO();

                await _unitOfWork.BookRepository.AddRangeAsync(books);

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException(ex.Message);
            }
        }

        public async Task<List<Book>> GetBooksAsync()
        {
            try
            {
                var books = await _unitOfWork.BookRepository.GetAllAsync();
                return books;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<BookViewModel> GetBooksByAuthorAsync(string author)
        {
            var books = _unitOfWork.BookRepository.FindListEntity(book => book.Author == author);
            if (!books.Any())
            {
                throw new Exception($"Books with Author: {author} can not be found!");
            }
            return books.ToBookViewModel();
        }

        public List<BookViewModel> GetBooksByGenreAsync(BookGenre genre)
        {
            var books = _unitOfWork.BookRepository.FindListEntity(book => book.Genre == genre);
            if (!books.Any())
            {
                throw new Exception($"Books with Genre: {genre} can not be found!");
            }
            return books.ToBookViewModel();
        }

        public List<BookViewModel> GetBooksByPublicationDateAsync(DateTime publicationDate)
        {
            var books = _unitOfWork.BookRepository.FindListEntity(book => book.PublicationDate == publicationDate);
            if (!books.Any())
            {
                throw new Exception($"Books with Publication Date: {publicationDate} can not be found!");
            }

            return books.ToBookViewModel();
        }

        public List<BookViewModel> GetBooksByTitleAsync(string title)
        {
            var books = _unitOfWork.BookRepository.FindListEntity(book => book.Title == title);

            if (!books.Any())
            {
                throw new Exception($"Books with Title: {title} can not be found!");
            }

            return books.ToBookViewModel();
        }

        //In case I need it for testing
        public async Task HardDelete(int bookId)
        {
            var book = await _unitOfWork.BookRepository.FindEntityAsync(book => book.Id.Equals(bookId));
            if (book is null)
            {
                throw new Exception($"Book with Id {bookId} cannot be found!");
            }
            _unitOfWork.BookRepository.HardDelete(book);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task SoftDelete(int bookId)
        {
            var bookToDelete = await _unitOfWork.BookRepository.FindEntityAsync(book => book.Id.Equals(bookId));
            if (bookToDelete is null)
            {
                throw new Exception($"Book with Id {bookId} cannot be found!");
            }
            _unitOfWork.BookRepository.SoftDelete(bookToDelete);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateBookAsync(int id, BookDTO book)
        {
            var updateBook = await _unitOfWork.BookRepository.FindEntityAsync(book => book.Id.Equals(id));
            if (updateBook is null)
            {
                throw new Exception($"Book with Id {id} cannot be found!");
            }

            updateBook.UpdateFromBookDTO(book, _currentTime.GetCurrentTime());

            await _unitOfWork.SaveChangesAsync();
        }
    }
}
