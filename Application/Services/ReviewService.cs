﻿using Application.DTO;
using Application.Utils;
using Application.Interfaces;

namespace Application.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddReview(ReviewDTO reviewDTO)
        {
            var review = reviewDTO.ToAddReview();
            await _unitOfWork.ReviewRepository.AddAsync(review);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<List<ReviewViewModel>> ViewReviews(int bookId)
        {
            var review = await _unitOfWork.ReviewRepository.ViewReviews(bookId);
            if (!review.Any())
            {
                throw new Exception("This book have no review!");
            }

            return review.ToReviewViewModel();
        }
    }
}
