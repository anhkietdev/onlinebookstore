﻿using Application.Interfaces;
using Application.Utils;
using Application.ViewModels;
using AutoMapper;

namespace Application.Services
{
    public class CartItemService : ICartItemService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CartItemService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddBookToCart(CartItemDTO cartItemAddDTO)
        {
            try
            {
                var book = await _unitOfWork.BookRepository.FindEntityAsync(book => book.Id.Equals(cartItemAddDTO.BookId));

                if (book is null)
                {
                    throw new Exception($"Book with Id : {cartItemAddDTO.BookId} not found");
                }

                var user = await _unitOfWork.UserRepository.FindEntityAsync(user => user.Id.Equals(cartItemAddDTO.UserId));
                if (user is null)
                {
                    throw new Exception($"User with Id : {cartItemAddDTO.UserId} not found");
                }

                if (book.StockQuantity < cartItemAddDTO.Quantity || book.StockQuantity == 0)
                {
                    throw new Exception($"Not enough Stock Quantity of Book: {book.Id}");
                }

                book.StockQuantity -= cartItemAddDTO.Quantity;

                var cartItem = cartItemAddDTO.ToCartItem();

                await _unitOfWork.CartItemRepository.AddAsync(cartItem);
                _unitOfWork.BookRepository.Update(book);

                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task RemoveBookFromCart(CartItemDTO cartItem)
        {
            var cartItems = _unitOfWork.CartItemRepository.ViewCartDetail(cartItem.UserId);
            if (cartItems is null || !cartItems.Any())
            {
                throw new Exception("Cart Items not found!");
            }

            // Tìm CartItem cần xóa
            var cartItemToRemove = cartItems.FirstOrDefault(item => item.BookId == cartItem.BookId);
            if (cartItemToRemove is null)
            {
                throw new Exception($"Cart Item for Book with Id {cartItem.BookId} not found in the cart!");
            }

            // Kiểm tra số lượng cần xóa
            if (cartItem.Quantity > cartItemToRemove.Quantity)
            {
                throw new Exception($"Not enough quantity of Book with Id {cartItem.BookId} in the cart!");
            }

            if (cartItem.Quantity == cartItemToRemove.Quantity)
            {
                // Nếu quantity cần xóa bằng với quantity trong CartItem, thì xóa luôn CartItem này
                _unitOfWork.CartItemRepository.SoftDelete(cartItemToRemove);
            }
            else
            {
                // Ngược lại, giảm số lượng cần xóa khỏi CartItem
                cartItemToRemove.Quantity -= cartItem.Quantity;
            }

            // Cập nhật lại StockQuantity cho sách
            var book = await _unitOfWork.BookRepository.GetBookByIdAsync(cartItem.BookId);
            if (book is null)
            {
                throw new Exception($"Book with Id {cartItem.BookId} not found!");
            }

            book.StockQuantity += cartItem.Quantity;

            await _unitOfWork.SaveChangesAsync();
        }

        public List<CartItemViewModel> ViewCartItemDetail(int userId)
        {
            var cartItems = _unitOfWork.CartItemRepository.ViewCartDetail(userId).ToList();

            if (cartItems.Count == 0)
            {
                throw new Exception("Cart Item not found!");
            }

            var cartItemViewModels = cartItems.ToCartItemViewModelList();
            return cartItemViewModels;
        }
    }
}
