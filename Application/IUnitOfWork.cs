﻿using Application.IRepositories;

namespace Application
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }
        public IBookRepository BookRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IOrderDetailRepository OrderDetailRepository { get; }
        public ICartItemRepository CartItemRepository { get; }
        public IFavoriteBookRepository FavoriteBookRepository { get; }
        public IOTPRepository OTPRepository { get; }
        Task<int> SaveChangesAsync();
    }
}
