﻿namespace Application.IRepositories
{
    public interface IFavoriteBookRepository : IGenericRepository<FavoriteBook>
    {
        List<FavoriteBook> ViewAllFavoriteBooks(int userId);
    }
}
