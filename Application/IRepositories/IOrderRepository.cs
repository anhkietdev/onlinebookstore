﻿namespace Application.IRepositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
        Task<int> GetOrderCountForToday();
        Task<int> GetPaidOrderCount();
        Task<int> TotalRevenue();
    }
}
