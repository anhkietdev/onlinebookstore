﻿using Application.ViewModels;
using Domain.Enums;

namespace Application.IRepositories
{
    public interface IBookRepository : IGenericRepository<Book>
    {
        Task<Book?> GetBookByIdAsync(int id);
    }
}
