﻿using Application.ViewModels;

namespace Application.IRepositories
{
    public interface ICartItemRepository : IGenericRepository<CartItem>
    {
        List<CartItem> ViewCartDetail(int userId);
    }
}
