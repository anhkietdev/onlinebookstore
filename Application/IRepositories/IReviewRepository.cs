﻿namespace Application.IRepositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        Task<List<Review>> ViewReviews(int bookId);
    }
}
