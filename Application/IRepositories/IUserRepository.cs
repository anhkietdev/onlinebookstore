﻿namespace Application.IRepositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserByUsernameAndPasswordHash(string username, string password);
        User GetUserByEmail(string email);
        Task<bool> IsExisted (string username);
        Task<bool> IsExistedEmail (string email);
        Task<int> NewAccountCount();
    }
}
