﻿using Application.Commons;
using System.Linq.Expressions;

namespace Application.IRepositories
{
    public interface IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        Task<List<TEntity>> GetAllAsync();
        Task<TEntity?> GetByIdAsync(int id);
        List<TEntity> FindListEntity(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity?> FindEntityAsync(Expression<Func<TEntity, bool>> predicate);
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(List<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(List<TEntity> entities);
        void SoftDelete(TEntity entity);
        void SoftDeleteRange(List<TEntity> entities);
        void HardDelete(TEntity entity);

        Task<Pagination<TEntity>> GetPagedDataAsync(int pageNumber = 0, int pageSize = 10);
    }
}
