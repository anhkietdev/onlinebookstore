﻿namespace Application.Commons
{
    public class AppConfiguration
    {
        public string DatabaseConnection { get; set; }
        public JWTSection JWTSection { get; set; }
        public EmailSection EmailSection { get; set; }

        public AppConfiguration()
        {
        }
    }
    public class JWTSection
    {
        public string SecretKey { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int ExpiresInMinutes { get; set; }
    }

    public class EmailSection
    {
        public string EmailString { get; set; }
        public string AppPassword { get; set; }
        public string OTPSubject { get; set; }
        public string OTPBody { get; set; }
        public string PaymentSubject { get; set; }
        public string PaymentBody { get; set; }
        public string ReportSubject { get; set; }
        public string ReportBody { get; set; }
    }
}
