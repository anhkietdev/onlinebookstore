﻿namespace Application.Commons
{
    public class Pagination<T>
    {
        public int TotalItems { get; set; }
        public int PageSize { get; set; } 
        public int TotalPages { get; set; } 
        public int PageIndex { get; set; }

        public bool HasNextPage => PageIndex < TotalPages - 1;

        public bool HasPreviousPage => PageIndex > 0;

        public ICollection<T> Items { get; set; }

        public Pagination(int totalItems, int pageSize, int pageIndex, ICollection<T> items)
        {
            TotalItems = totalItems;
            PageSize = pageSize;
            PageIndex = pageIndex;
            Items = items;
            CalculateTotalPages();
        }

        private void CalculateTotalPages()
        {
            TotalPages = (int)Math.Ceiling((double)TotalItems / PageSize);
        }
    }
}
