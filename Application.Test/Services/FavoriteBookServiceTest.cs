﻿using Application.DTO;
using Application.Services;
using Moq;
using System.Linq.Expressions;

namespace Application.Test.Services
{
    public class FavoriteBookServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly FavoriteBookService _favoriteBookService;

        public FavoriteBookServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _favoriteBookService = new FavoriteBookService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async Task AddFavoriteBook_WithValidUserAndBook_AddsFavoriteBookSuccessfully()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 1,
                BookId = 101 // Assuming valid BookId
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(new User { Id = favoriteBookDTO.UserId });

            _unitOfWorkMock.Setup(x => x.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>()))
                           .ReturnsAsync(new Book { Id = favoriteBookDTO.BookId });

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
                           .ReturnsAsync((FavoriteBook)null);

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()))
                           .Returns(Task.CompletedTask);

            // Act
            await _favoriteBookService.Add(favoriteBookDTO);

            // Assert
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task AddFavoriteBook_WithInvalidUser_ThrowsException()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 2,
                BookId = 101 // Assuming valid BookId
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync((User)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.Add(favoriteBookDTO));

            _unitOfWorkMock.Verify(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task AddFavoriteBook_WithExistingFavoriteBook_ThrowsException()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 1,
                BookId = 101 // Assuming valid BookId
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(new User { Id = favoriteBookDTO.UserId });

            _unitOfWorkMock.Setup(x => x.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>()))
                           .ReturnsAsync(new Book { Id = favoriteBookDTO.BookId });

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
                           .ReturnsAsync(new FavoriteBook());

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.Add(favoriteBookDTO));

            _unitOfWorkMock.Verify(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()), Times.Once);

            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.AddAsync(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task RemoveFavoriteBook_WithValidUserAndBook_RemovesFavoriteBookSuccessfully()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 1,
                BookId = 101 // Assuming valid BookId
            };

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
                .ReturnsAsync(new FavoriteBook { UserId = favoriteBookDTO.UserId, BookId = favoriteBookDTO.BookId });

            // Act
            await _favoriteBookService.Remove(favoriteBookDTO);

            // Assert
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.SoftDelete(It.IsAny<FavoriteBook>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task RemoveFavoriteBook_WithInvalidUser_ThrowsException()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 2,
                BookId = 101 // Assuming valid BookId
            };

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
               .ReturnsAsync((FavoriteBook)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.Remove(favoriteBookDTO));

            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()), Times.Once);

            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.SoftDelete(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task RemoveFavoriteBook_WithInvalidBook_ThrowsException()
        {
            // Arrange
            var favoriteBookDTO = new FavoriteBookDTO
            {
                UserId = 1,
                BookId = 102 // Assuming invalid BookId
            };

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
               .ReturnsAsync((FavoriteBook)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.Remove(favoriteBookDTO));

            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()), Times.Once);

            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.SoftDelete(It.IsAny<FavoriteBook>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task ViewFavoriteBook_WithInvalidUser_ThrowsException()
        {
            // Arrange
            int userId = 2;

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
               .ReturnsAsync((FavoriteBook)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _favoriteBookService.ViewFavoriteBook(userId));

            // Verify that FindEntityAsync was called with the correct user ID
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()), Times.Once);

            // Verify that other methods were not called
            _unitOfWorkMock.Verify(x => x.FavoriteBookRepository.ViewAllFavoriteBooks(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public async Task ViewFavoriteBook_WithNoFavoriteBooks_ReturnsEmptyList()
        {
            // Arrange
            int userId = 1;

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.FindEntityAsync(It.IsAny<Expression<Func<FavoriteBook, bool>>>()))
                            .ReturnsAsync(new FavoriteBook { UserId = userId });

            _unitOfWorkMock.Setup(x => x.FavoriteBookRepository.ViewAllFavoriteBooks(userId))
                           .Returns(new List<FavoriteBook>()); // Empty list

            // Act
            Assert.ThrowsAsync<Exception>(() => _favoriteBookService.ViewFavoriteBook(userId));
        }
    }
}
