﻿using Application.DTO;
using Application.Services;
using Moq;

namespace Application.Test.Services
{
    public class ReviewServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly ReviewService _reviewService;

        public ReviewServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _reviewService = new ReviewService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async Task AddReview_Success()
        {
            // Arrange
            var reviewDTO = new ReviewDTO
            {
                BookId = 1,
                Image = "aa",
                Content = "aa",
                Rating = 5
            };

            _unitOfWorkMock.Setup(u => u.ReviewRepository.AddAsync(It.IsAny<Review>()))
                .Returns(Task.CompletedTask);

            // Act
            await _reviewService.AddReview(reviewDTO);

            // Assert
            _unitOfWorkMock.Verify(x => x.ReviewRepository.AddAsync(It.IsAny<Review>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task AddReview_Failure()
        {
            // Arrange
            var reviewDTO = new ReviewDTO
            {
                BookId = 1,
                Image = "aa",
                Content = "aa",
                Rating = 5
            };

            var expectedExceptionMessage = "Failed to add review.";

            _unitOfWorkMock.Setup(u => u.ReviewRepository.AddAsync(It.IsAny<Review>()))
                .Throws(new Exception(expectedExceptionMessage));

            // Act & Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _reviewService.AddReview(reviewDTO));
            Assert.Equal(expectedExceptionMessage, exception.Message);

            _unitOfWorkMock.Verify(x => x.ReviewRepository.AddAsync(It.IsAny<Review>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task ViewReviews_WithInvalidBookId_ThrowsException()
        {
            // Arrange
            int invalidBookId = 2;
            _unitOfWorkMock.Setup(x => x.ReviewRepository.ViewReviews(invalidBookId)).ReturnsAsync(new List<Review>());

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _reviewService.ViewReviews(invalidBookId));
        }

        [Fact]
        public async Task ViewReviews_WithValidBookId_ReturnsReviewViewModelList()
        {
            // Arrange
            int validBookId = 1;
            var mockReviews = new List<Review>(); // Assume no reviews for this test
            _unitOfWorkMock.Setup(x => x.ReviewRepository.ViewReviews(validBookId)).ReturnsAsync(mockReviews);

            // Act & Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _reviewService.ViewReviews(validBookId));
            Assert.Equal("This book have no review!", exception.Message);

            _unitOfWorkMock.Verify(x => x.ReviewRepository.ViewReviews(validBookId), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }
    }
}
