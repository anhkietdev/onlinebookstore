﻿using Application.DTO;
using Application.Interfaces;
using Application.Services;
using Moq;
using System.Linq.Expressions;

namespace Application.Test.Services
{
    public class OrderServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly OrderService _orderService;
        private readonly Mock<IEmailUtils> _emailUtilsMock;

        public OrderServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _emailUtilsMock = new Mock<IEmailUtils>();

            _orderService = new OrderService(_unitOfWorkMock.Object, _emailUtilsMock.Object);
        }

        [Fact]
        public async Task AddToOrderDetail_WithValidUser_AddsOrderDetailsAndDeletesCartItems()
        {
            // Arrange
            int userId = 1;
            var order = new Order { /* provide necessary properties for order */ };
            var user = new User { Id = userId /* provide necessary properties for user */ };
            var cartItems = new List<CartItem> { /* create mock cart items */ };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(user);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.ViewCartDetail(userId))
                           .Returns(cartItems);

            _unitOfWorkMock.Setup(x => x.OrderDetailRepository.AddRangeAsync(It.IsAny<List<OrderDetail>>()))
                           .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.SoftDeleteRange(It.IsAny<List<CartItem>>()));

            // Act
            await _orderService.AddToOrderDetail(userId, order);

            // Assert
            _unitOfWorkMock.Verify(x => x.OrderDetailRepository.AddRangeAsync(It.IsAny<List<OrderDetail>>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.CartItemRepository.SoftDeleteRange(It.IsAny<List<CartItem>>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task AddToOrderDetail_WithInvalidUser_ThrowsException()
        {
            // Arrange
            int userId = 2;
            var order = new Order { /* provide necessary properties for order */ };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync((User)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _orderService.AddToOrderDetail(userId, order));

            _unitOfWorkMock.Verify(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()), Times.Once);

            _unitOfWorkMock.Verify(x => x.OrderDetailRepository.AddRangeAsync(It.IsAny<List<OrderDetail>>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.CartItemRepository.SoftDeleteRange(It.IsAny<List<CartItem>>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task AddToOrderDetail_WithExceptionDuringCartItemDeletion_RollsBackChanges()
        {
            // Arrange
            int userId = 1;
            var order = new Order { /* provide necessary properties for order */ };
            var user = new User { Id = userId /* provide necessary properties for user */ };
            var cartItems = new List<CartItem> { /* create mock cart items */ };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(user);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.ViewCartDetail(userId))
                           .Returns(cartItems);

            _unitOfWorkMock.Setup(x => x.OrderDetailRepository.AddRangeAsync(It.IsAny<List<OrderDetail>>()))
                           .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.SoftDeleteRange(It.IsAny<List<CartItem>>()))
                           .Throws(new Exception("Error deleting cart items"));

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _orderService.AddToOrderDetail(userId, order));

            // Verify that Rollback (SaveChangesAsync not called) due to the exception during cart item deletion
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task ProcessPayment_WithValidUserAndSufficientWallet_ProcessesPaymentSuccessfully()
        {
            // Arrange
            int userId = 1;
            var orderDTO = new OrderDTO
            {
                // provide necessary properties for orderDTO
            };

            var user = new User
            {
                Id = userId,
                WalletAmount = orderDTO.TotalPrice + 1 // User has enough money
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(user);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.ViewCartDetail(userId))
                           .Returns(new List<CartItem> { /* create mock cart items */ });

            _unitOfWorkMock.Setup(x => x.OrderRepository.AddAsync(It.IsAny<Order>()))
                           .Returns(Task.CompletedTask);

            _unitOfWorkMock.Setup(x => x.SaveChangesAsync())
                           .ReturnsAsync(1);

            // Act
            var result = await _orderService.ProcessPayment(userId, orderDTO);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(userId, result.UserId);
            Assert.Equal(orderDTO.TotalPrice, result.TotalPrice);
            Assert.True(result.PaidDate > DateTime.MinValue);

            Assert.Equal(user.WalletAmount - orderDTO.TotalPrice, user.WalletAmount);

            _unitOfWorkMock.Verify(x => x.OrderRepository.AddAsync(It.IsAny<Order>()), Times.Once);
            _emailUtilsMock.Verify(x => x.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task ProcessPayment_WithInvalidUser_ThrowsException()
        {
            // Arrange
            int userId = 2;
            var orderDTO = new OrderDTO
            {
                // provide necessary properties for orderDTO
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync((User)null);

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _orderService.ProcessPayment(userId, orderDTO));

            _unitOfWorkMock.Verify(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()), Times.Once);

            _unitOfWorkMock.Verify(x => x.OrderRepository.AddAsync(It.IsAny<Order>()), Times.Never);
            _emailUtilsMock.Verify(x => x.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }

        [Fact]
        public async Task ProcessPayment_WithInsufficientWallet_ThrowsException()
        {
            // Arrange
            int userId = 1;
            var orderDTO = new OrderDTO
            {
                // provide necessary properties for orderDTO
            };

            var user = new User
            {
                Id = userId,
                WalletAmount = orderDTO.TotalPrice - 1 // User does not have enough money
            };

            _unitOfWorkMock.Setup(x => x.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(user);

            _unitOfWorkMock.Setup(x => x.CartItemRepository.ViewCartDetail(userId))
                           .Returns(new List<CartItem> { /* create mock cart items */ });

            // Act & Assert
            await Assert.ThrowsAsync<Exception>(() => _orderService.ProcessPayment(userId, orderDTO));

            _unitOfWorkMock.Verify(x => x.OrderRepository.AddAsync(It.IsAny<Order>()), Times.Never);
            _emailUtilsMock.Verify(x => x.SendEmailAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _unitOfWorkMock.Verify(x => x.SaveChangesAsync(), Times.Never);
        }
    }
}
