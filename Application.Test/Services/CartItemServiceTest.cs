﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels;
using Domain.Enums;
using Moq;
using System;
using System.Linq.Expressions;

namespace Application.Test.Services
{
    public class CartItemServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        
        private readonly CartItemService _cartItemService;

        public CartItemServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _cartItemService = new CartItemService(_unitOfWorkMock.Object);
        }

        [Fact]
        public async Task AddBookToCart_SuccessfullyAddsBookToCart()
        {
            // Arrange
            var validBookId = 1;
            var validUserId = 1;
            var validQuantity = 1;

            var existingBook = new Book
            {
                Id = validBookId,
                Title = "Book1",
                Author = "Author1",
                PublicationDate = DateTime.Now.AddDays(-1),
                StockQuantity = 5, // Sufficient stock quantity
                PricePerUnit = 30,
                Genre = BookGenre.Mystery
            };

            var existingUser = new User
            {
                Id = validUserId,
                Username = "User1",
                PasswordHash = "password",
                Role = Role.Customer,
                Email = "user1@example.com"                
            };

            var cartItemDTO = new CartItemDTO
            {
                BookId = validBookId,
                UserId = validUserId,
                Quantity = validQuantity
            };

            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync(existingBook);
            _unitOfWorkMock.Setup(u => u.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>())).ReturnsAsync(existingUser);
            _unitOfWorkMock.Setup(u => u.CartItemRepository.FindEntityAsync(It.IsAny<Expression<Func<CartItem, bool>>>())).ReturnsAsync((CartItem)null);

            // Act
            await _cartItemService.AddBookToCart(cartItemDTO);

            // Assert
            _unitOfWorkMock.Verify(u => u.CartItemRepository.AddAsync(It.IsAny<CartItem>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task AddBookToCart_ThrowsExceptionForInvalidBookId()
        {
            // Arrange
            var invalidBookId = 100; // An invalid book id
            var validUserId = 100;
            var validQuantity = 2;

            var cartItemDTO = new CartItemDTO
            {
                BookId = invalidBookId,
                UserId = validUserId,
                Quantity = validQuantity
            };

            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync((Book)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _cartItemService.AddBookToCart(cartItemDTO));
            Assert.Equal($"Book with Id : {invalidBookId} not found", exception.Message);
        }

        [Fact]
        public async Task RemoveBookFromCart_SuccessfullyRemovesBookFromCart()
        {
            // Arrange
            var validBookId = 1;
            var validUserId = 100;
            var validQuantityInCart = 5;

            var existingCartItem = new CartItem
            {
                Id = 1,
                BookId = validBookId,
                UserId = validUserId,
                Quantity = validQuantityInCart
            };

            var cartItemDTO = new CartItemDTO
            {
                BookId = validBookId,
                UserId = validUserId,
                Quantity = 2
            };

            _unitOfWorkMock.Setup(u => u.CartItemRepository.ViewCartDetail(It.IsAny<int>())).Returns(new List<CartItem> { existingCartItem });
            _unitOfWorkMock.Setup(u => u.BookRepository.GetBookByIdAsync(It.IsAny<int>())).ReturnsAsync(new Book { Id = validBookId, StockQuantity = 10 });

            // Act
            await _cartItemService.RemoveBookFromCart(cartItemDTO);

            // Assert
            _unitOfWorkMock.VerifyAll();
        }


        [Fact]
        public async Task RemoveBookFromCart_ThrowsExceptionForInvalidCartItem()
        {
            // Arrange
            var invalidBookId = 100; // An invalid book id
            var validUserId = 4;

            var cartItemDTO = new CartItemDTO
            {
                BookId = invalidBookId,
                UserId = validUserId,
                Quantity = 2
            };

            _unitOfWorkMock.Setup(u => u.CartItemRepository.ViewCartDetail(It.IsAny<int>())).Returns(new List<CartItem>());
            _unitOfWorkMock.Setup(u => u.BookRepository.GetBookByIdAsync(It.IsAny<int>())).ReturnsAsync((Book)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _cartItemService.RemoveBookFromCart(cartItemDTO));
            Assert.Equal("Cart Items not found!", exception.Message);
        }

        

        [Fact]
        public void ViewCartItemDetail_NoCartItemsFound_ThrowsException()
        {
            // Arrange
            var userId = 1;

            // Simulate no cart items found
            _unitOfWorkMock.Setup(u => u.CartItemRepository.ViewCartDetail(userId)).Returns(new List<CartItem>());

            // Act and Assert
            Assert.Throws<Exception>(() => _cartItemService.ViewCartItemDetail(userId));
        }
    }
}
