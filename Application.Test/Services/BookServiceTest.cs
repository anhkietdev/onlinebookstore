﻿using Application.Interfaces;
using Application.Services;
using Application.ViewModels;
using Domain.Enums;
using Moq;
using System.Linq.Expressions;

namespace Application.Test.Services
{
    public class BookServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly Mock<ICurrentTime> _currentTimeMock;
        private readonly BookService _bookService;

        public BookServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _currentTimeMock = new Mock<ICurrentTime>();

            _bookService = new BookService(_unitOfWorkMock.Object, _currentTimeMock.Object);
        }

        [Fact]
        public async Task AddAsync_ShouldAddBookSuccessfully()
        {
            // Arrange
            var bookDTO = new BookDTO
            {
                Title = "testTitle",
                Author = "testAuthor",
                PublicationDate = new DateTime(2000, 10, 10),
                Genre = 0,
                StockQuantity = 100,
                PricePerUnit = 100
            };

            _unitOfWorkMock.Setup(u => u.BookRepository.AddRangeAsync(It.IsAny<List<Book>>()))
                .Returns(Task.CompletedTask);


            // Act
            await _bookService.AddAsync(bookDTO);

            // Assert
            _unitOfWorkMock.Verify(u => u.BookRepository.AddRangeAsync(It.IsAny<List<Book>>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }



        [Fact]
        public async Task AddAsync_NullBookList_ShouldThrowException()
        {
            // Act and Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => _bookService.AddAsync(null));
        }

        [Fact]
        public async Task AddAsync_NullBookInList_ShouldThrowException()
        {
            // Arrange
            var bookListWithNull = new BookDTO
            {
                // Missing required fields
            };

            // Act and Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => _bookService.AddAsync(bookListWithNull));
        }

        [Fact]
        public async Task GetBooksAsync_ReturnsEmptyListWhenNoBooks()
        {
            // Arrange
            _unitOfWorkMock.Setup(u => u.BookRepository.GetAllAsync()).ReturnsAsync(new List<Book>());

            // Act
            var result = await _bookService.GetBooksAsync();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<Book>>(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBooksAsync_ReturnsNullOnException()
        {
            // Arrange
            _unitOfWorkMock.Setup(u => u.BookRepository.GetAllAsync()).ThrowsAsync(new Exception("Test exception"));

            // Act
            var result = await _bookService.GetBooksAsync();

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void GetBooksByAuthorAsync_ReturnsBooksForValidAuthor()
        {
            // Arrange
            var author = "testAuthor";
            var expectedBooks = new List<Book>
            {
                new Book
                {
                    Id = 1,
                    Title = "testTitle",
                    Author = author,
                    PublicationDate = DateTime.Now.AddDays(-10),
                    StockQuantity = 20,
                    PricePerUnit = 30,
                    Genre = 0,
                }
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(expectedBooks);

            // Act
            var result = _bookService.GetBooksByAuthorAsync(author);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BookViewModel>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetBooksByAuthorAsync_ThrowsExceptionForInvalidAuthor()
        {
            // Arrange
            var invalidAuthor = "NonExistentAuthor";
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(new List<Book>());

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => _bookService.GetBooksByAuthorAsync(invalidAuthor));
            Assert.Equal($"Books with Author: {invalidAuthor} can not be found!", exception.Message);
        }

        [Fact]
        public void GetBooksByGenreAsync_ReturnsBooksForValidGenre()
        {
            // Arrange
            var expectedBooks = new List<Book>
            {
                new Book
                {
                    Id = 1,
                    Title = "testAuthor",
                    Author = "Test",
                    PublicationDate = DateTime.Now.AddDays(-10),
                    StockQuantity = 20,
                    PricePerUnit = 30,
                    Genre = 0,
                }
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(expectedBooks);

            // Act
            var result = _bookService.GetBooksByGenreAsync(0);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BookViewModel>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetBooksByGenreAsync_ThrowsExceptionForInvalidGenre()
        {
            // Arrange
            var invalidGenre = (BookGenre)100; // An invalid genre value
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(new List<Book>());

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => _bookService.GetBooksByGenreAsync(invalidGenre));
            Assert.Equal($"Books with Genre: {invalidGenre} can not be found!", exception.Message);
        }

        [Fact]
        public void GetBooksByPublicationDateAsync_ReturnsBooksForValidDate()
        {
            // Arrange
            var publicDate = DateTime.Now.AddDays(-5);
            var expectedBooks = new List<Book>
            {
                new Book
                {
                    Id = 1,
                    Title = "testTitle",
                    Author = "testAuthor",
                    PublicationDate = publicDate,
                    StockQuantity = 20,
                    PricePerUnit = 30,
                    Genre = 0,
                }
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(expectedBooks);

            // Act
            var result = _bookService.GetBooksByPublicationDateAsync(publicDate);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BookViewModel>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetBooksByPublicationDateAsync_ThrowsExceptionForInvalidDate()
        {
            // Arrange
            var invalidPublicationDate = DateTime.Now.AddDays(10); // An invalid date in the future
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(new List<Book>());

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => _bookService.GetBooksByPublicationDateAsync(invalidPublicationDate));
            Assert.Equal($"Books with Publication Date: {invalidPublicationDate} can not be found!", exception.Message);
        }

        [Fact]
        public void GetBooksByTitleAsync_ReturnsBooksForValidTitle()
        {
            // Arrange
            var testTitle = "testTitle";
            var expectedBooks = new List<Book>
            {
                new Book
                {
                    Id = 1,
                    Title = testTitle,
                    Author = "testAuthor",
                    PublicationDate = DateTime.Now.AddDays(-1),
                    StockQuantity = 100,
                    PricePerUnit = 100,
                    Genre = 0,
                }
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(expectedBooks);

            // Act
            var result = _bookService.GetBooksByTitleAsync(testTitle);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BookViewModel>>(result);
            Assert.NotEmpty(result);
        }

        [Fact]
        public void GetBooksByTitleAsync_ThrowsExceptionForInvalidTitle()
        {
            // Arrange
            var invalidTitle = "NonExistentTitle";
            _unitOfWorkMock.Setup(u => u.BookRepository.FindListEntity(It.IsAny<Expression<Func<Book, bool>>>())).Returns(new List<Book>());

            // Act and Assert
            var exception = Assert.Throws<Exception>(() => _bookService.GetBooksByTitleAsync(invalidTitle));
            Assert.Equal($"Books with Title: {invalidTitle} can not be found!", exception.Message);
        }

        [Fact]
        public async Task SoftDelete_DeletesBookForValidId()
        {
            // Arrange
            var validBookId = 1;
            var bookToDelete = new Book
            {
                Id = validBookId,
                Title = "Book1",
                Author = "Author1",
                PublicationDate = DateTime.Now,
                StockQuantity = 20,
                PricePerUnit = 30,
                Genre = BookGenre.Mystery,
                Reviews = new List<Review>(),
                OrderDetails = new List<OrderDetail>()
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync(bookToDelete);

            // Act
            await _bookService.SoftDelete(validBookId);

            // Assert
            _unitOfWorkMock.Verify(u => u.BookRepository.SoftDelete(It.IsAny<Book>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task SoftDelete_ThrowsExceptionForInvalidId()
        {
            // Arrange
            var invalidBookId = 100; // An invalid book id
            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync((Book)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.SoftDelete(invalidBookId));
            Assert.Equal($"Book with Id {invalidBookId} cannot be found!", exception.Message);
        }

        [Fact]
        public async Task HardDelete_DeletesBookForValidId()
        {
            // Arrange
            var validBookId = 1;
            var bookToDelete = new Book
            {
                Id = validBookId,
                Title = "Book1",
                Author = "Author1",
                PublicationDate = DateTime.Now,
                StockQuantity = 20,
                PricePerUnit = 30,
                Genre = BookGenre.Mystery,
                Reviews = new List<Review>(),
                OrderDetails = new List<OrderDetail>()
            };
            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync(bookToDelete);

            // Act
            await _bookService.HardDelete(validBookId);

            // Assert
            _unitOfWorkMock.Verify(u => u.BookRepository.HardDelete(It.IsAny<Book>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task HardDelete_ThrowsExceptionForInvalidId()
        {
            // Arrange
            var invalidBookId = 100; // An invalid book id
            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync((Book)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.HardDelete(invalidBookId));
            Assert.Equal($"Book with Id {invalidBookId} cannot be found!", exception.Message);
        }

        [Fact]
        public async Task UpdateBookAsync_UpdatesBookForValidId()
        {
            // Arrange
            var validBookId = 1;
            var updatedBookDTO = new BookDTO
            {
                Title = "UpdatedTitle",
                Author = "UpdatedAuthor",
                PublicationDate = DateTime.Now.AddDays(-5),
                StockQuantity = 50,
                PricePerUnit = 40,
                Genre = BookGenre.Mystery
            };

            var existingBook = new Book
            {
                Id = validBookId,
                Title = "OriginalTitle",
                Author = "OriginalAuthor",
                PublicationDate = DateTime.Now.AddDays(-10),
                StockQuantity = 20,
                PricePerUnit = 30,
                Genre = BookGenre.Mystery,
            };

            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync(existingBook);

            // Act
            await _bookService.UpdateBookAsync(validBookId, updatedBookDTO);

            // Assert
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
            Assert.Equal(updatedBookDTO.Title, existingBook.Title);
            Assert.Equal(updatedBookDTO.Author, existingBook.Author);
            Assert.Equal(updatedBookDTO.PublicationDate, existingBook.PublicationDate);
            Assert.Equal(updatedBookDTO.StockQuantity, existingBook.StockQuantity);
            Assert.Equal(updatedBookDTO.PricePerUnit, existingBook.PricePerUnit);
            Assert.Equal(updatedBookDTO.Genre, existingBook.Genre);
        }

        [Fact]
        public async Task UpdateBookAsync_ThrowsExceptionForInvalidId()
        {
            // Arrange
            var invalidBookId = 100; // An invalid book id
            var updatedBookDTO = new BookDTO
            {
                Title = "UpdatedTitle",
                Author = "UpdatedAuthor",
                PublicationDate = DateTime.Now.AddDays(-5),
                StockQuantity = 50,
                PricePerUnit = 40,
                Genre = BookGenre.Romance
            };

            _unitOfWorkMock.Setup(u => u.BookRepository.FindEntityAsync(It.IsAny<Expression<Func<Book, bool>>>())).ReturnsAsync((Book)null);

            // Act and Assert
            var exception = await Assert.ThrowsAsync<Exception>(() => _bookService.UpdateBookAsync(invalidBookId, updatedBookDTO));
            Assert.Equal($"Book with Id {invalidBookId} cannot be found!", exception.Message);
        }
    }
}
