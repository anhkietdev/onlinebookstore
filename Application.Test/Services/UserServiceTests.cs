﻿using Application.Commons;
using Application.Interfaces;
using Application.Services;
using Application.Utils;
using Application.ViewModels;
using Domain.Entities;
using Domain.Enums;
using Microsoft.IdentityModel.Tokens;
using Moq;
using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;

namespace Application.Test.Services
{
    public class UserServiceTests
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;
        private readonly AppConfiguration _appConfiguration;
        private readonly Mock<ICurrentTime> _currentTimeMock;
        private readonly Mock<IEmailUtils> _emailUtilsMock;
        private readonly UserService _userService;

        public UserServiceTests()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _appConfiguration = new AppConfiguration
            {
                JWTSection = new JWTSection
                {
                    SecretKey = "ThisIs128BitSecretKey123456789AK"
                },
                EmailSection = new EmailSection
                {
                    EmailString = "truonganhkietdev@gmail.com",
                    AppPassword = "pcpxpggokmexvyal"
                }
            };
            _currentTimeMock = new Mock<ICurrentTime>();
            _emailUtilsMock = new Mock<IEmailUtils>();

            _userService = new UserService(_unitOfWorkMock.Object, _appConfiguration, _currentTimeMock.Object, _emailUtilsMock.Object);
        }

        [Fact]
        public async Task LoginAsync_ValidUser_ReturnsToken()
        {
            // Arrange
            var user = new UserLoginDTO { Username = "adminn", Password = "123" };
            var dbUser = new User { Id = 5, Role = Role.Admin };
            var now = DateTime.UtcNow;

            _unitOfWorkMock.Setup(u => u.UserRepository.GetUserByUsernameAndPasswordHash(user.Username, user.Password.Hash())).ReturnsAsync(dbUser);
            _currentTimeMock.Setup(t => t.GetCurrentTime()).Returns(now);

            // Act
            var token = await _userService.LoginAsync(user);

            // Assert
            Assert.NotNull(token);

            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appConfiguration.JWTSection.SecretKey)),
                ValidateIssuer = false,
                ValidateAudience = false
            };

            var principal = tokenHandler.ValidateToken(token, validationParameters, out var validatedToken);
            var claimIdentity = (ClaimsIdentity)principal.Identity;

            Assert.Equal(dbUser.Id.ToString(), claimIdentity.Claims.First(c => c.Type == ClaimTypes.NameIdentifier).Value);
            Assert.Equal(dbUser.Role.ToString(), claimIdentity.Claims.First(c => c.Type == ClaimTypes.Role).Value);
        }
        [Fact]
        public async Task Login_InvalidUser_ShouldThrowException()
        {
            // Arrange
            var user = new UserLoginDTO
            {
                Username = "adminn",
                Password = "123"
            };

            // Set up the GetUserByUsernameAndPasswordHash to return null
            _unitOfWorkMock.Setup(u => u.UserRepository.GetUserByUsernameAndPasswordHash(user.Username, user.Password.Hash()));

            // Act
            await Assert.ThrowsAsync<Exception>(() => _userService.LoginAsync(user));

            // Assert
        }

        [Fact]
        public async Task RegisterAsync_ValidUser_AddsUserToRepository()
        {
            // Arrange
            var user = new UserRegisterDTO { Username = "testUsername", Password = "testPassword" };

            _unitOfWorkMock.Setup(u => u.UserRepository.IsExisted(user.Username)).ReturnsAsync(false);

            // Act
            await _userService.RegisterAsync(user);

            // Assert
            _unitOfWorkMock.Verify(u => u.UserRepository.AddAsync(It.IsAny<User>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task Register_InvalidUser_ShouldThrowException()
        {
            //Arrange
            var user = new UserRegisterDTO { Username = "admin", Password = "123" };

            _unitOfWorkMock.Setup(u => u.UserRepository.IsExisted(user.Username)).ReturnsAsync(true);

            await Assert.ThrowsAsync<Exception>(() => _userService.RegisterAsync(user));
        }

        [Fact]
        public async Task RegisterAdmin_ValidUser_AddsAdminToRepository()
        {
            // Arrange
            var validAdmin = new UserRegisterDTO
            {
                Username = "newadmin",
                Password = "adminpassword",
                Name = "New Admin",
                Address = "Admin Address",
                Email = "admin@example.com",
                PhoneNumber = "123456789"
            };

            _unitOfWorkMock.Setup(u => u.UserRepository.IsExisted(validAdmin.Username)).ReturnsAsync(false);

            // Act
            await _userService.RegisterAdminAsync(validAdmin);

            // Assert
            _unitOfWorkMock.Verify(u => u.UserRepository.AddAsync(It.IsAny<User>()), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task RegisterAdmin_ExistingUsername_ShouldThrowException()
        {
            // Arrange
            var existingAdmin = new UserRegisterDTO
            {
                Username = "existingadmin",
                Password = "adminpassword",
                Name = "Existing Admin",
                Address = "Admin Address",
                Email = "existingadmin@example.com",
                PhoneNumber = "987654321"
            };

            _unitOfWorkMock.Setup(u => u.UserRepository.IsExisted(existingAdmin.Username)).ReturnsAsync(true);

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => _userService.RegisterAdminAsync(existingAdmin));
        }

        [Fact]
        public async Task RegisterAdmin_InvalidUser_ShouldThrowException()
        {
            // Arrange
            var invalidAdmin = new UserRegisterDTO
            {
                // Missing required fields
            };

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => _userService.RegisterAdminAsync(invalidAdmin));
        }



        [Fact]
        public async Task ForgotPassword_ValidEmail_SendsOTP()
        {
            // Arrange
            var userDto = new ForgotPasswordDTO { Email = "truonganhkietdev@gmail.com" };

            _unitOfWorkMock.Setup(repo => repo.UserRepository.IsExistedEmail(userDto.Email)).ReturnsAsync(true);
            _unitOfWorkMock.Setup(repo => repo.OTPRepository.AddAsync(It.IsAny<OTP>())).Returns(Task.CompletedTask);

            _currentTimeMock.Setup(time => time.GetCurrentTime()).Returns(DateTime.Now);

            // Act
            await _userService.ForgotPassword(userDto);

            // Assert
            _emailUtilsMock.Verify(emailUtils => emailUtils.SendEmailAsync(userDto.Email, "ForgotPassword"), Times.Once);
        }

        [Fact]
        public async Task ResetPassword_ValidInput_ResetsPasswordAndDeletesOTP()
        {
            var invalidOTP = "invalidOTP";
            var email = "testuser@example.com";
            var newPassword = "newpassword";

            _unitOfWorkMock.Setup(repo => repo.OTPRepository.GetOTPByEmailAndCode(email, invalidOTP)).Returns((OTP)null);

            await Assert.ThrowsAsync<Exception>(() => _userService.ResetPassword(invalidOTP, email, newPassword));
        }

        [Fact]
        public async Task ResetPassword_UserNotFound_ShouldThrowException()
        {
            var invalidOTP = "invalidOTP";
            var notFoundEmail = "notfound@example.com";

            _unitOfWorkMock.Setup(repo => repo.OTPRepository.GetOTPByEmailAndCode(notFoundEmail, invalidOTP)).Returns((OTP)null);
            _unitOfWorkMock.Setup(repo => repo.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync((User)null);

            await Assert.ThrowsAsync<Exception>(() => _userService.ResetPassword(invalidOTP, notFoundEmail, "newpassword"));
        }

        [Fact]
        public async Task ResetPassword_ExpiredOTP_ShouldThrowException()
        {
            var expiredOTP = "expiredOTP";
            var email = "testuser@example.com";

            var expiredOTPEntity = new OTP { ExpirationTime = DateTime.Now.AddDays(-1) };
            _unitOfWorkMock.Setup(repo => repo.OTPRepository.GetOTPByEmailAndCode(email, expiredOTP)).Returns(expiredOTPEntity);

            await Assert.ThrowsAsync<Exception>(() => _userService.ResetPassword(expiredOTP, email, "newpassword"));
        }

        [Fact]
        public async Task WalletRecharge_ValidUserAndMoney_AddsMoneyToWallet()
        {
            // Arrange
            var userId = 1;
            var initialWalletAmount = 100;
            var rechargeAmount = 50;

            var existingUser = new User { Id = userId, WalletAmount = initialWalletAmount };
            _unitOfWorkMock.Setup(u => u.UserRepository.FindEntityAsync(It.IsAny<Expression<Func<User, bool>>>()))
                           .ReturnsAsync(existingUser);

            // Act
            await _userService.WalletRecharge(rechargeAmount, userId);

            // Assert
            Assert.Equal(initialWalletAmount + rechargeAmount, existingUser.WalletAmount);
            _unitOfWorkMock.Verify(u => u.UserRepository.Update(existingUser), Times.Once);
            _unitOfWorkMock.Verify(u => u.SaveChangesAsync(), Times.Once);
        }

        [Fact]
        public async Task WalletRecharge_InvalidUserId_ShouldThrowException()
        {
            // Arrange
            var invalidUserId = -1;

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => _userService.WalletRecharge(50, invalidUserId));
        }

        [Fact]
        public async Task WalletRecharge_NonPositiveMoney_ShouldThrowException()
        {
            // Arrange
            var userId = 1;

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => _userService.WalletRecharge(0, userId));
        }
    }
}
