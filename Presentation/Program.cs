using Application.Commons;
using Application.Services;
using Hangfire;
using Hangfire.MemoryStorage;
using Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Presentation;
using Presentation.Middlewares;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

var configuration = builder.Configuration.Get<AppConfiguration>();

builder.Services.AddInfrastructureServices(configuration.DatabaseConnection);
builder.Services.AddWebAPIService();
builder.Services.AddSingleton(configuration);

builder.Services.AddHangfire(config => config.UseMemoryStorage());
builder.Services.AddHangfireServer();


// Add services to the container.
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration.JWTSection.Issuer,
            ValidAudience = configuration.JWTSection.Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration.JWTSection.SecretKey)),
        };
    });


var app = builder.Build();

app.UseMiddleware<GlobalExceptionMiddleware>();

app.UseHangfireDashboard();
app.UseStaticFiles();
app.MapHealthChecks("/healthchecks");

RecurringJob.AddOrUpdate<DailyReportService>("Send report to admin", report => report.GenerateDailyReport(), "59 23 * * *");

app.MapControllers();

app.Run();


