﻿using Application.Interfaces;
using Application.ViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class BookController : BaseController
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddBookAsync([FromBody] BookDTO bookAddDTO)
        {
            await _bookService.AddAsync(bookAddDTO);

            return Ok("Added Book Successfully!");
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateBookAsync(int id, [FromBody] BookDTO bookAddDTO)
        {
            await _bookService.UpdateBookAsync(id, bookAddDTO);

            return Ok("Updated Book Successfully!");
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteBookAsync(int id)
        {
            await _bookService.SoftDelete(id);

            return Ok("Delete Book Successfully!");
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PermanentDeleteAsync(int id)
        {
            await _bookService.HardDelete(id);

            return Ok($"Permanent Delete Book with ID : {id}");
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooksAsync()
        {
            var books = await _bookService.GetBooksAsync();

            return Ok(books.ToList());
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByTitleAsync([FromForm] string title)
        {
            var books = _bookService.GetBooksByTitleAsync(title);

            return Ok(books);
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByAuthorAsync([FromForm] string author)
        {
            var books = _bookService.GetBooksByAuthorAsync(author);

            return Ok(books);
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByGenreAsync([FromForm] BookGenre genre)
        {

            var books =  _bookService.GetBooksByGenreAsync(genre);

            return Ok(books);
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByPublicationYear([FromForm] DateTime publicationYear)
        {
            var books = _bookService.GetBooksByPublicationDateAsync(publicationYear);

            return Ok(books);
        }
    }
}
