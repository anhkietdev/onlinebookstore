﻿using Application.Interfaces;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IClaimsService _claimsService;

        public UserController(IUserService userService, IClaimsService claimsService)
        {
            _userService = userService;
            _claimsService = claimsService;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterDTO userRegister)
        {
            await _userService.RegisterAsync(userRegister);

            return Ok("Register Customer successfully!");
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAdminAsync([FromBody] UserRegisterDTO userRegister)
        {
            await _userService.RegisterAdminAsync(userRegister);

            return Ok("Register Admin successfully!");
        }

        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginDTO userLogin)
        {
            var jwt = await _userService.LoginAsync(userLogin);

            return Ok(jwt);
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDTO user)
        {
            try
            {
                await _userService.ForgotPassword(user);
                return Ok("An OTP email has been sent successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed to send OTP email: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword([FromBody] ForgotPasswordDTO resetPasswordDTO)
        {
            try
            {
                await _userService.ResetPassword(resetPasswordDTO.OTP, resetPasswordDTO.Email, resetPasswordDTO.NewPassword);
                return Ok("Reset Password successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> WalletRecharge([FromForm] int money)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);

            await _userService.WalletRecharge(money, userId);

            return Ok($"Add {money}$ for User {userId} successful!");
        }
    }
}
