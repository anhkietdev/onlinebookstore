﻿using Application.DTO;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class FavoriteBookController : BaseController
    {
        private readonly IFavoriteBookService _favoriteBookService;
        private readonly IClaimsService _claimsService;

        public FavoriteBookController(IFavoriteBookService favoriteBookService, IClaimsService claimsService)
        {
            _favoriteBookService = favoriteBookService;
            _claimsService = claimsService;
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> AddFavoriteBook([FromForm] FavoriteBookDTO favoriteBookDTO)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            favoriteBookDTO.UserId = userId;

            await _favoriteBookService.Add(favoriteBookDTO);

            return Ok($"Added Book {favoriteBookDTO.BookId} to favorite book of User {favoriteBookDTO.UserId}");
        }

        [HttpDelete]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> RemoveFavoriteBook([FromForm] FavoriteBookDTO favoriteBookDTO)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            favoriteBookDTO.UserId = userId;

            await _favoriteBookService.Remove(favoriteBookDTO);

            return Ok($"Delete Book {favoriteBookDTO.BookId} from Favorite Book");
        }

        [HttpGet]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> ViewAllFavoriteBook()
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);

            var listFavoriteBooks = await _favoriteBookService.ViewFavoriteBook(userId);

            return Ok(listFavoriteBooks);
        }
    }
}
