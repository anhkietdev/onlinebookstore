﻿using Application.Interfaces;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class CartItemController : BaseController
    {
        private readonly ICartItemService _cartItemService;
        private readonly IClaimsService _claimsService;

        public CartItemController(ICartItemService cartItemService, IClaimsService claimsService)
        {
            _cartItemService = cartItemService;
            _claimsService = claimsService;
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> AddBookToCartAsync([FromForm] CartItemDTO cartItemAddDTO)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            cartItemAddDTO.UserId = userId;

            await _cartItemService.AddBookToCart(cartItemAddDTO);

            return Ok($"Added {cartItemAddDTO.Quantity} of Book(ID: {cartItemAddDTO.BookId}) for User(ID: {cartItemAddDTO.UserId})");
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> RemoveBookFromCartAsync([FromForm] CartItemDTO cartItemDTO)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            cartItemDTO.UserId = userId;

            await _cartItemService.RemoveBookFromCart(cartItemDTO);

            return Ok($"Remove {cartItemDTO.Quantity} of Book {cartItemDTO.BookId} from Cart");
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> ViewCartAsync()
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            var cartDetail = _cartItemService.ViewCartItemDetail(userId);

            return Ok(cartDetail);
        }
    }
}
