﻿using Application.DTO;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        private readonly IClaimsService _claimsService;

        public OrderController(IOrderService orderService, IClaimsService claimsService)
        {
            _orderService = orderService;
            _claimsService = claimsService;
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> ProcessPaymentAsync()
        {
            OrderDTO orderDTO = new OrderDTO();

            var userId = _claimsService.GetCurrentUserId(HttpContext);

            var order = await _orderService.ProcessPayment(userId, orderDTO);

            await _orderService.AddToOrderDetail(userId, order);

            return Ok($"Payment for Order {order.Id} with {order.TotalPrice}$ successful");
        }
    }
}
