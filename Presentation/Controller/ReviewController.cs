﻿using Application.DTO;
using Application.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class ReviewController : BaseController
    {
        private readonly IReviewService _reviewService;
        private readonly IClaimsService _claimsService;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ReviewController(IReviewService reviewService, IClaimsService claimsService, IWebHostEnvironment hostingEnvironment)
        {
            _reviewService = reviewService;
            _claimsService = claimsService;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> AddReviewAsync([FromForm] ReviewDTO reviewDTO)
        {
            var userId = _claimsService.GetCurrentUserId(HttpContext);
            reviewDTO.UserId = userId;

            var formFile = HttpContext.Request.Form.Files["Image"];

            if (formFile is not null && formFile.Length > 0)
            {
                string uniqueFileName = $"book_{reviewDTO.BookId}.jpg"; // Đặt tên ảnh dựa trên BookId

                string webRootPath = _hostingEnvironment.WebRootPath; // Đường dẫn tới thư mục wwwroot
                string filePath = Path.Combine(webRootPath, uniqueFileName); // Đường dẫn tới tệp đã tải lên

                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await formFile.CopyToAsync(stream);
                }

                reviewDTO.Image = $"/{uniqueFileName}"; // Đường dẫn từ thư mục gốc của wwwroot
            }

            await _reviewService.AddReview(reviewDTO);

            return Ok($"Added Review for BookID {reviewDTO.BookId} successfully!");
        }




        [HttpGet]
        public async Task<IActionResult> ViewReviews([FromForm] int bookId)
        {
            var review = await _reviewService.ViewReviews(bookId);

            return Ok(review);
        }
    }
}
