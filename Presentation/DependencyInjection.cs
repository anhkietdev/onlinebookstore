﻿using Application.Interfaces;
using Application.Services;
using Presentation.Middlewares;

namespace Presentation
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService (this IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<GlobalExceptionMiddleware>();
            services.AddScoped<IClaimsService, ClaimsService>();
            services.AddScoped<IDailyReportService, DailyReportService>();

            services.AddHealthChecks();

            return services;
        }
    }
}
