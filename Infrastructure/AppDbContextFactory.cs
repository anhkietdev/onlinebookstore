﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Infrastructure
{
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Data Source=KIETTA;Database=ProjectGraduation;Trusted_Connection=True;TrustServerCertificate=True;User Id=kietta;Password=123");

            return new AppDbContext(optionsBuilder.Options);
        }
    }
}
