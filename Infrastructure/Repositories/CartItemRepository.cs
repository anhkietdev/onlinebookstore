﻿using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class CartItemRepository : GenericRepository<CartItem>, ICartItemRepository
    {
        private readonly AppDbContext _context;

        public CartItemRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public  List<CartItem> ViewCartDetail(int userId) => _dbSet.Include(cart => cart.User)
                                                                   .Include(cart => cart.Book)
                                                                   .Where(cart => cart.UserId == userId)    
                                                                   .ToList();
    }
}
