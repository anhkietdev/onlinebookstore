﻿using Application.Interfaces;
using Application.IRepositories;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class BookRepository : GenericRepository<Book>, IBookRepository
    {
        private readonly AppDbContext _context;
        public BookRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public async Task<Book?> GetBookByIdAsync(int id) => await _context.Books.FirstOrDefaultAsync(x => x.Id == id);
    }
}
