﻿using Application.Interfaces;
using Application.IRepositories;
using Domain.Entities;

namespace Infrastructure.Repositories
{

    internal class OTPRepository : GenericRepository<OTP>, IOTPRepository
    {
        private readonly AppDbContext _context;
        public OTPRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public OTP GetOTPByEmailAndCode(string email, string code)
        {
            return _context.OTP.FirstOrDefault(otp => otp.Email == email && otp.Code == code);
        }
    }
}
