﻿using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class FavoriteBookRepository : GenericRepository<FavoriteBook>, IFavoriteBookRepository
    {
        private readonly AppDbContext _context;

        public FavoriteBookRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public List<FavoriteBook> ViewAllFavoriteBooks(int userId) => _dbSet.Include(fBook => fBook.User)
                                                                             .Include(fBook => fBook.Book)
                                                                             .Where(fBook => fBook.UserId == userId)
                                                                             .Where(fBook => fBook.IsDeleted == false)
                                                                             .ToList();
    }
}
