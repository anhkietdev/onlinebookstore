﻿using Application.Commons;
using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructure.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseEntity
    {
        protected DbSet<TEntity> _dbSet;
        private readonly ICurrentTime _timeService;

        public GenericRepository(AppDbContext context, ICurrentTime timeService)
        {
            _dbSet = context.Set<TEntity>();
            _timeService = timeService;
        }

        public Task<List<TEntity>> GetAllAsync() => _dbSet.ToListAsync();

        public async Task<TEntity?> GetByIdAsync(int id) => await _dbSet.FirstOrDefaultAsync(x => x.Equals(id));

        public async Task AddAsync(TEntity entity)
        {
            entity.CreatedOn = _timeService.GetCurrentTime();
            await _dbSet.AddAsync(entity);
        }

        public async Task AddRangeAsync(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.CreatedOn = _timeService.GetCurrentTime();
            }
            await _dbSet.AddRangeAsync(entities);
        }

        public async Task<Pagination<TEntity>> GetPagedDataAsync(int pageNumber = 0, int pageSize = 10)
        {
            var query = _dbSet.AsQueryable();

            var totalItems = await query.CountAsync();

            var items = await query.Skip(pageNumber * pageSize)
                                  .Take(pageSize)
                                  .AsNoTracking()
                                  .ToListAsync();

            var pagination = new Pagination<TEntity>(totalItems, pageSize, pageNumber, items);

            return pagination;
        }


        public void SoftDelete(TEntity entity)
        {
            entity.IsDeleted = true;
            entity.DeletedOn = _timeService.GetCurrentTime();
            _dbSet.Update(entity);
        }

        public void SoftDeleteRange(List<TEntity> entities)
        {
            var currentTime = _timeService.GetCurrentTime();
            foreach (var entity in entities)
            {
                entity.DeletedOn = currentTime;
                entity.IsDeleted = true;
            }
            _dbSet.UpdateRange(entities);
        }

        public void Update(TEntity entity)
        {
            entity.ModifiedOn = _timeService.GetCurrentTime();
            _dbSet.Update(entity);
        }

        public void UpdateRange(List<TEntity> entities)
        {
            var currentTime = _timeService.GetCurrentTime();
            foreach (var entity in entities)
            {
                entity.ModifiedOn = currentTime;
            }
            _dbSet.UpdateRange(entities);
        }

        public  List<TEntity> FindListEntity(Expression<Func<TEntity, bool>> predicate) => _dbSet.Where(predicate).ToList();

        public void HardDelete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public Task<TEntity?> FindEntityAsync(Expression<Func<TEntity, bool>> predicate) =>  _dbSet.FirstOrDefaultAsync(predicate);
    }
}
