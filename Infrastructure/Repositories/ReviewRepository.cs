﻿using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        private readonly AppDbContext _context;
        public ReviewRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public Task<List<Review>> ViewReviews(int bookId)
        {
            var reviews = _dbSet.Include(user => user.User).Include(book => book.Book).Where(review => review.BookId == bookId);
            return reviews.ToListAsync();
        }
    }
}
