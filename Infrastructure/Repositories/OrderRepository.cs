﻿using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class OrderRepository : GenericRepository<Order>, IOrderRepository
    {
        private readonly AppDbContext _context;
        public OrderRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public async Task<int> GetOrderCountForToday()
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = today.AddDays(1);

            int orderCount = await _context.Orders.Where(order => order.CreatedOn >= today && order.CreatedOn < tomorrow).CountAsync();

            return orderCount;
        }

        public async Task<int> GetPaidOrderCount()
        {
            int paidOrderCount = await _context.Orders.Where(order => order.PaidDate > DateTime.MinValue).CountAsync();

            return paidOrderCount;
        }

        public async Task<int> TotalRevenue()
        {
            var sum =  await _context.Orders.SumAsync(order => order.TotalPrice);

            return sum;
        }
    }
}
