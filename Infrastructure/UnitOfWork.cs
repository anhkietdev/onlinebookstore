﻿using Application;
using Application.IRepositories;

namespace Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly ICartItemRepository _cartItemRepository;
        private readonly IFavoriteBookRepository _favoriteBookRepository;
        private readonly IOTPRepository _otpRepository;

        public UnitOfWork(AppDbContext context, IUserRepository userRepository,
                                                IBookRepository bookRepository,
                                                IReviewRepository reviewRepository,
                                                IOrderRepository orderRepository,
                                                IOrderDetailRepository orderDetailRepository,
                                                ICartItemRepository cartItemRepository,
                                                IFavoriteBookRepository favoriteBookRepository, 
                                                IOTPRepository otpRepository)
        {
            _context = context;
            _userRepository = userRepository;
            _bookRepository = bookRepository;
            _reviewRepository = reviewRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _cartItemRepository = cartItemRepository;
            _favoriteBookRepository = favoriteBookRepository;
            _otpRepository = otpRepository;
        }

        public IUserRepository UserRepository => _userRepository;

        public IBookRepository BookRepository => _bookRepository;

        public IReviewRepository ReviewRepository => _reviewRepository;

        public IOrderRepository OrderRepository => _orderRepository;

        public IOrderDetailRepository OrderDetailRepository => _orderDetailRepository;

        public ICartItemRepository CartItemRepository => _cartItemRepository;

        public IFavoriteBookRepository FavoriteBookRepository => _favoriteBookRepository;

        public IOTPRepository OTPRepository => _otpRepository;

        public Task<int> SaveChangesAsync() => _context.SaveChangesAsync();
    }
}
