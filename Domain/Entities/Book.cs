﻿using Domain.Enums;

public class Book : BaseEntity
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Author { get; set; }
    public DateTime PublicationDate { get; set; }
    public int StockQuantity { get; set; }
    public int PricePerUnit { get; set; }
    public BookGenre Genre { get; set; }
    public ICollection<Review> Reviews { get; set; }
    public ICollection<OrderDetail> OrderDetails { get; set; }
}

