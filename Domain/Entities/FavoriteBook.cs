﻿public class FavoriteBook : BaseEntity
{
    public int BookId { get; set; }
    public Book Book { get; set; }
    public User User { get; set; }
    public int UserId { get; set; }
}

