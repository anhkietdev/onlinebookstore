﻿using Domain.Enums;

public class User : BaseEntity
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string PasswordHash { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public DateTime DateOfBirth { get; set; }
    public string Address { get; set; }
    public string PhoneNumber { get; set; }
    public int WalletAmount { get; set; }
    public Role Role { get; set; }
    public ICollection<Order> Orders { get; set; }
    public ICollection<FavoriteBook> FavoriteBooks { get; set;}
    public ICollection<CartItem> CartItems { get; set; }
}

