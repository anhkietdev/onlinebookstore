﻿namespace Domain.Entities
{
    public class DailyReport
    {
        public DateTime Date { get; set; }
        public int TotalOrdersPlaced { get; set; }
        public int TotalOrdersPaid { get; set; }
        public decimal TotalRevenue { get; set; }
        public int NewUsersCount { get; set; }
    }

}
